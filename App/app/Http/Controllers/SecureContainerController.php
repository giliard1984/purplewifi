<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class SecureContainerController extends Controller
{
    protected $range;

    // This is the main method called to render the initial page
    public function index($data = null) 
    {
        return view('adventofcode', ['data' => $data]);
    }

    public function process(Request $request) 
    {
        if ($request->get('advents') === "day4_1") {

            self::secure_container($request->get('puzzleInput'));
            return self::index($this->range);

        } elseif ($request->get('advents') === "day4_2") {
            
            $part_one = self::secure_container($request->get('puzzleInput'));
            $part_one['part_two'] = self::secure_container_part_two($part_one);

            return self::index($part_one);
        }
    }

    private function secure_container($input = null)
    {
        // sets the range (min and max values)
        $this->range = self::range($input);

        $validation = Validator::make($this->range, [
            'min' => [
                'required',
                'size:6'
            ],
            'max' => [
                'required',
                'size:6'
            ],
        ]);

        // if password is not 6-digits long, then fail
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation->errors());
        }

        // iterate through the given range
        for ($x = $this->range['min']; $x <= $this->range['max']; $x++) {

            // assumes that the password is valid
            $isPasswordValid = true;

            // iterate through characters
            $characters = str_split((string)$x);

            // checking if the next character is lower than the current one. if so, skip the decrease process
            for ($y = 0; $y < sizeof($characters) - 1; $y++) {
                if ($characters[$y] > $characters[$y + 1]) {
                    $isPasswordValid = false; // it is not a valid password
                    continue;  
                }
            }

            // if the valid password variable says it is not valid, then goes to the next password
            if (!$isPasswordValid) continue;

            // two adjacent digits are the same
            for ($i = 0; $i < sizeof($characters) - 1; $i++) {

                if ($characters[$i] == $characters[$i + 1] ) {
                    array_push($this->range['passwords'], $x); 
                    break;
                }

            }

        }

        return $this->range;

    }

    private function secure_container_part_two($data = [])
    {
        $new_array = [];

        for ($x = 0; $x < sizeof($data['passwords']); $x++) {

            // iterate through characters
            $characters = str_split((string)$data['passwords'][$x]);

            $is_valid = false;

            // gets a password and iterate through each digit
            for ($y = 0; $y < sizeof($characters) - 1; $y++) {

                if (
                    substr_count( (string)$data['passwords'][$x] , $characters[$y] ) == 2
                ) {
                    array_push($new_array, $data['passwords'][$x]);
                    break;
                }

            }

        }

        return $new_array;

    }

    private function range($input = null) 
    {
        // generates an array by splitting the values
        $range = explode('-',$input);

        return [
            'possibilities' => $range[1] - $range[0],
            'min' => (integer)$range[0],
            'max' => (integer)$range[1],
            'passwords' => []
        ];

    }

}