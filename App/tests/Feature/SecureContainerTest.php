<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SecureContainerTest extends TestCase
{
    // checking if main route exists
    public function testHomeRouteExists()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    // checking if process route exists
    public function testProcessRouteExists()
    {
        $response = $this->post('process');
        $this->assertEquals(200, $response->status());
    }

    /**
     * @depends testProcessRouteExists
     */
    public function testPositiveDay4Part1AssertEquals()
    {
        // calls the process endpoint passing the expected range
        $response = $this->call('POST', 'process', ['advents' => 'day4_1', 'puzzleInput' => '197487-673251']);
        $this->assertEquals(200, $response->status());

        // extract data from response
        $data = $response->getOriginalContent()->getData();

        // Is this range comprised of 475764 possibilities? this is the total
        $this->assertEquals($data['data']['possibilities'], 475764);

        // validate if part 1 returs 1640 possible passwords
        $this->assertEquals(sizeof($data['data']['passwords']), 1640);

    }

    /**
     * @depends testPositiveDay4Part1AssertEquals
     */
    public function testPostiveDay4Part2AssertEquals()
    {
        // calls the process endpoint passing the expected range
        $response = $this->call('POST', 'process', ['advents' => 'day4_2', 'puzzleInput' => '197487-673251']);
        $this->assertEquals(200, $response->status());

        // extract data from response
        $data = $response->getOriginalContent()->getData();

        // Is this range comprised of 475764 possibilities? this is the total
        $this->assertEquals($data['data']['possibilities'], 475764);

        // validate if part 1 returs 1640 possible passwords
        $this->assertEquals(sizeof($data['data']['part_two']), 1126);

    }

    /**
     * @depends testProcessRouteExists
     */
    public function testNegativeDay4Part1ForAssertNotEquals()
    {
        // calls the process endpoint passing an unexpected range
        $response = $this->call('POST', 'process', ['advents' => 'day4_1', 'puzzleInput' => '389487-653121']);

        // extract data from response
        $data = $response->getOriginalContent()->getData();

        // Is this range not comprised of 475764 possibilities?
        $this->assertNotEquals($data['data']['possibilities'], 475764);

        // validate if part 1 returs something different of 1640
        $this->assertNotEquals(sizeof($data['data']['passwords']), 1640);

    }

}
