<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Advent of Code</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            
            <div class="content">
                <div class="title m-b-md">
                    Advent of Code 2019
                </div>

                <form action="/process" method="post">
                    <div class="links">
                        <label for="cars">Choose a day:</label>

                        <select id="advents" name="advents">
                            <option value="day4_1">Day 4 Part 1 (Secure Container)</option>
                            <option value="day4_2">Day 4 Part 2 (Secure Container)</option>
                        </select>   

                        <label for="fname">Your puzzle input is:</label>
                        <input type="text" id="puzzleInput" name="puzzleInput" placeholder="Please type in the puzzle input"><br><br>

                        <input type="submit" value="Submit" >

                    </div>
                </form>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <br />
                    @foreach ($errors->all() as $error)
                    <div>{{ $error }}</div>
                    @endforeach
                </div><br />
                @endif

                @if(isset($data))
                <div>
                    <b>[Part 1] The answer is {{ sizeof($data['passwords']) }}</b>
                </div>
                @if(isset($data['part_two']))
                <div>
                    <b>[Part 2] The answer is {{ sizeof($data['part_two']) }}</b>
                </div>
                @endif
                @endif
            </div>
        </div>
    </body>
</html>
